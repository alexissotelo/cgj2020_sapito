﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sapito.Main
{

    public enum RoundPhase
    {
        aim, shot, scoring
    }


    public class RoundManager : MonoBehaviour
    {
        static RoundManager instance;
        public static RoundManager Instance { get { return instance; } }

        public static Action<RoundPhase> OnPhaseChanged;

        public RoundPhase Phase { get => roundPhase; }

        RoundPhase roundPhase;

        [SerializeField] GameData gameData;

        [SerializeField] CoinBehavior coinBehavior;

        //
        public UIRound uiRound;
        public UIScoreParticle scoreParticle;

        public GameObject nextRound;
        public GameObject gameoverWindow;

        //
        public const int AMOUNT_ROUNDS = 10;
        public const int AMOUNT_PLAYERS = 2;

        void Awake()
        {
            instance = this;

            Init_Game( AMOUNT_PLAYERS , AMOUNT_ROUNDS );
        }

        void Init_Game(int _amountPlayers, int _amountRounds)
        {
            for ( int iPlayer = 0; iPlayer < _amountPlayers; iPlayer++ )
            {
                gameData.players.Add( new PlayerData { playerName = "Jugador " + ( iPlayer + 1 ) } );
            }

            gameData.Set_TotalRounds( _amountRounds );

            gameData.currentPlayer = 0;
            gameData.currentRound = 0;

            uiRound.Init_UIRound( gameData );

            coinBehavior.enabled = true;
        }

        public void Reset_Game()
        {
            gameoverWindow.SetActive( false );

            Init_Game( AMOUNT_PLAYERS , AMOUNT_ROUNDS );

            Start_Round();
        }

        void Start()
        {
            Start_Round();
        }

        public void Start_Round()
        {
            gameData.currentPlayer = gameData.currentRound % gameData.players.Count;

            uiRound.Update_CurrentPlayer( gameData.currentPlayer );

            uiRound.Update_RoundHeader( "Ronda " + gameData.Get_RoundProgress() );

            Start_AimPhase();
        }

        void Update()
        {
            if ( Input.GetKeyDown( KeyCode.R ) )
                Start_AimPhase();
        }

        void Start_AimPhase()
        {
            roundPhase = RoundPhase.aim;
            OnPhaseChanged?.Invoke( roundPhase );

            nextRound.SetActive( false );
        }

        public void Start_ShotPhase()
        {
            if ( roundPhase == RoundPhase.aim )
            {
                roundPhase = RoundPhase.shot;

                OnPhaseChanged?.Invoke( roundPhase );
            }
        }

        public void Start_ScorePhase( int _score )
        {
            if ( roundPhase == RoundPhase.shot )
            {
                roundPhase = RoundPhase.scoring;

                Debug.Log( "Start_ScorePhase >> " + _score );

                gameData.Add_Round( _score , gameData.currentPlayer );

                //Update Score UI
                //if(_score > 0)
                    scoreParticle.Show_Score( _score );

                int totalScore = gameData.Calculate_PlayerTotalScore(gameData.currentPlayer);
                uiRound.Update_Scores( gameData.currentPlayer , totalScore );

                //Next Round
                gameData.currentRound++;

                Handle_RoundOver();

                OnPhaseChanged?.Invoke( roundPhase );
            }
        }

        void Handle_RoundOver()
        {
            if(gameData.currentRound >= gameData.totalRounds)
            {
                gameoverWindow.SetActive( true );

                uiRound.Update_RoundHeader( "Resultados");
            }
            else
            {
                nextRound.SetActive( true );
            }
        }
    }

}