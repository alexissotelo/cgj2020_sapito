﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoundData
{
    public int playerID;
    public int score;

    public override string ToString()
    {
        return "playerID: " + playerID + ", score: " + score;
    }
}
