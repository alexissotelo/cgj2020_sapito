﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public List<PlayerData> players = new List<PlayerData>();

    public List<RoundData> rounds = new List<RoundData>();

    public int totalRounds;

    public int currentRound;

    public int currentPlayer;

    public int status;//waiting, created, running, finished, cancelled

    public void Set_TotalRounds(int _rounds)
    {
        totalRounds = _rounds * players.Count;

        //for ( int iRound = 0; iRound < _rounds; iRound++ )
        //{
        //    for ( int iPlayer = 0; iPlayer < players.Count; iPlayer++ )
        //    {
        //        rounds.Add( new RoundData { player = players[ iPlayer ].playerName , score = 0 } );
        //    }
        //}
    }

    public void Add_Round(int _score, int _player)
    {
        rounds.Add( new RoundData 
        { 
            playerID =  _player ,
            score = _score 
        } );

        Debug.Log( "Add_Round >> " + this.ToString() );
    }

    public PlayerData CurrentPlayer
    {
        get { return players[ currentPlayer ]; }
    }

    public int Calculate_PlayerTotalScore(int _playerID)
    {
        var playerRounds = rounds.Where( r => r.playerID == _playerID ).ToList();

        int totalScore = 0;
        for ( int iRound = 0; iRound < playerRounds.Count; iRound++ )
        {
            totalScore += playerRounds[ iRound ].score;
        }

        return totalScore;
    }

    public override string ToString()
    {
        string msg = "GameData\n";

        for ( int iRound = 0; iRound < rounds.Count; iRound++ )
        {
            msg += "Round " + iRound + ": " + rounds[ iRound ].ToString()+"\n";
        }

        return msg;
    }

    public string Get_RoundProgress()
    {
        int playerCount = players.Count;
        string msg = ( currentRound / playerCount + 1 ) + "/" + (totalRounds / playerCount);
        return msg;
    }
}
