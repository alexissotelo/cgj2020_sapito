﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowCamera : MonoBehaviour
{
    public CinemachineVirtualCamera throwCamera;
    public CinemachineSmoothPath dollyPath;

    CinemachineTrackedDolly trackedDolly;

    float pathMax;

    float pathPosition = 1f;

    public float moveCameraSensitivity = 10f;

    void Start()
    {
        trackedDolly = throwCamera.GetCinemachineComponent<CinemachineTrackedDolly>();

        pathMax = dollyPath.m_Waypoints.Length;
    }

    void Update()
    {
        pathPosition += Input.GetAxis("Horizontal") * moveCameraSensitivity * 0.01f;
        pathPosition = Mathf.Clamp( pathPosition , 0 , pathMax );

        trackedDolly.m_PathPosition = pathPosition;
    }
}
