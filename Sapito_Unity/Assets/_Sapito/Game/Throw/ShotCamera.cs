﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sapito.Main
{

    public class ShotCamera : MonoBehaviour
    {
        public GameObject shotCamera;

        void Start()
        {
            RoundManager.OnPhaseChanged += OnRoundPhaseChanged;

        }

        void OnDestroy()
        {
            RoundManager.OnPhaseChanged -= OnRoundPhaseChanged;
        }

        void OnRoundPhaseChanged( RoundPhase _phase )
        {
            Toggle_Camera( _phase != RoundPhase.aim );
        }

        void Toggle_Camera(bool _on)
        {
            shotCamera.SetActive( _on );
        }
    }

}
