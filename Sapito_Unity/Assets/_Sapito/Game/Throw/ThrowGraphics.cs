﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowGraphics : MonoBehaviour
{
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] float lineTimeLength = 1.25f;//how far in to future will it show
    
    const int amountPoints = 10;
    const float pointDelta = 1f / amountPoints;

    public void Show_ThrowLine(
        Vector3 _startPosition , Vector3 _throwDirection , float _strength )
    {
        lineRenderer.enabled = true;

        Vector3[] positions = new Vector3[ amountPoints ];
        for ( int i = 0; i < amountPoints; i++ )
        {
            float time = i * pointDelta * lineTimeLength;
            float timeSqr = time * time;

            positions[ i ] = ( Physics.gravity * 0.5f * timeSqr )
                + ( _throwDirection * _strength * time )
                + _startPosition;
        }

        lineRenderer.SetPositions( positions );
    }

    public void Hide_ThrowLine()
    {
        lineRenderer.enabled = false;
    }
}
