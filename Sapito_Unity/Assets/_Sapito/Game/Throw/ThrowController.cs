﻿using Sapito.Main;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Sapito.Main;

public class ThrowController : MonoBehaviour
{
    public CoinBehavior coinRigid;
    public ThrowGraphics throwGraphics;

    [SerializeField] Vector2 throwForceScale = Vector2.one;
    [SerializeField] float minThrowForce = 1f;
    [SerializeField] float maxThrowForce = 3.5f;

    public float horizontalMouseDrag = 100f;
    public float verticalMouseDrag = 100f;
    public float dragRadius = 0.25f;

    Vector3 startMousePosition;
    Vector3 endDragPosition;

    float forceRatio;

    [Space()]
    public LineRenderer lineRenderer;
    public Gradient dragColor;

    bool dragStarted;


    void OnDrag_Start(  )
    {
        dragStarted = true;
        startMousePosition = Input.mousePosition;
    }

    void OnDrag_Update( )
    {
        if ( ! dragStarted )
            return;

        Vector3 mouseDelta = startMousePosition - Input.mousePosition;

        Vector2 delta;
        delta.x = Mathf.Clamp( mouseDelta.x / horizontalMouseDrag , -1f, 1f );
        delta.y = Mathf.Clamp( mouseDelta.y / verticalMouseDrag, -1f, 1f );
        delta.y = Mathf.Clamp01( delta.y );
        delta = Vector2.ClampMagnitude( delta , 1f );

        endDragPosition.x = transform.position.x - delta.x * dragRadius;
        endDragPosition.y = transform.position.y;
        endDragPosition.z = transform.position.z - delta.y * dragRadius;

        forceRatio = delta.y;

        lineRenderer.startColor = dragColor.Evaluate( forceRatio );
        lineRenderer.endColor = dragColor.Evaluate( forceRatio );

        lineRenderer.SetPosition( 0 , transform.position );
        lineRenderer.SetPosition( 1 , endDragPosition );
    }

    void OnDrag_End(  )
    {
        if(dragStarted)
        {
            dragStarted = false;

            coinRigid.Throw_Coin( Get_ThrowVector() * Calculate_Strength() );

            throwGraphics.Hide_ThrowLine();
            lineRenderer.SetPosition( 1 , transform.position );
        }
    }

    void Update_Input()
    {
        if ( Input.GetButtonDown( "Fire1" ) )
            OnDrag_Start( );

        if ( Input.GetButtonUp( "Fire1" ) )
            OnDrag_End( );
        
        if(Input.GetButton("Fire1") )
            OnDrag_Update( );
    }

    void Update()
    {
        Update_Input();

        Update_ThrowLine();
    }

    void Update_ThrowLine()
    {
        if ( dragStarted )
        {
            float str = Calculate_Strength();

            throwGraphics.Show_ThrowLine(
                transform.position ,
                Get_ThrowVector() ,
                str );
        }
    }

    float Calculate_Strength()
    {
        float mass = coinRigid.GetMass();
        float strength = Mathf.Lerp( minThrowForce , maxThrowForce , forceRatio ) / mass;

        return strength;
    }

    Vector3 Get_ThrowVector()
    {
        Vector3 fwd = ( transform.position - endDragPosition).normalized;
        //Vector3 fwd = Vector3.ClampMagnitude( transform.position - endDragPosition, 1f);

        //Vector3 fwd = transform.forward;
        fwd.y = 0f;
        return fwd * throwForceScale.x + Vector3.up * throwForceScale.y;
    }

    void OnRoundPhaseChanged(RoundPhase _phase)
    {
        if ( _phase == RoundPhase.aim )
        {
            dragStarted = false;
            enabled = true;
        }
        else
        {
            enabled = false;
        }
    }

    void Start()
    {
        RoundManager.OnPhaseChanged += OnRoundPhaseChanged;
    }

    void OnDestroy()
    {
        RoundManager.OnPhaseChanged -= OnRoundPhaseChanged;
    }
}
