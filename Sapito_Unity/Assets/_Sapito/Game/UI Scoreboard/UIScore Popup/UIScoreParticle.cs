﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIScoreParticle : MonoBehaviour
{
    public Animator animator;
    public TextMeshPro scoreText;

    public void Show_Score(int _score)
    {
        scoreText.text = _score + "!";

        animator.Play("Appear", 0 , 0f );
    }

}
