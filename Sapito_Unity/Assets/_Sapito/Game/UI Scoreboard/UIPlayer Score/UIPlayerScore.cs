﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIPlayerScore : MonoBehaviour
{
    [SerializeField] Animator animator;

    [SerializeField] TextMeshProUGUI nameLabel;
    [SerializeField] TextMeshProUGUI scoreLabel;

    public void Init_PlayerScore(string _playerName, int _score)
    {
        if( string.IsNullOrEmpty(_playerName) )
        {
            gameObject.SetActive( false );
            return;
        }

        nameLabel.text = _playerName;
        Update_PlayerScore( _score );
    }

    public void Update_Selected(bool _b)
    {
        animator.SetBool( "Selected" , _b );
    }

    public void Update_PlayerScore(int _score)
    {
        scoreLabel.text = _score.ToString();
    }

}
