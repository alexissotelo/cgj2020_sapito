﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIRound : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI roundLabel;

    [SerializeField] UIPlayerScore[] playerScores;

    int currentPlayer;

    public void Init_UIRound(GameData _gameData)
    {
        for ( int iPlayer = 0; iPlayer < playerScores.Length; iPlayer++ )
        {
            if ( iPlayer < _gameData.players.Count )
                playerScores[ iPlayer ].Init_PlayerScore( _gameData.players[ iPlayer ].playerName , 0 );
            else
                playerScores[ iPlayer ].Init_PlayerScore( null , -1 );
        }
    }

    public void Update_RoundHeader(string _msg)
    {
        roundLabel.text = _msg;
    }

    public void Update_CurrentPlayer( int _playerIndex )
    {
        playerScores[ currentPlayer ].Update_Selected( false );
        
        currentPlayer = _playerIndex;
        playerScores[ currentPlayer ].Update_Selected( true );
    }

    public void Update_Scores(int _playerIndex, int _score)
    {
        playerScores[ _playerIndex ].Update_PlayerScore( _score );
    }
}
