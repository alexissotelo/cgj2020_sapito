﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sapito.Main
{
    public class CoinBehavior : MonoBehaviour
    {
        [SerializeField] Rigidbody coinRigid;
        [SerializeField] CoinSound coinSound;

        Vector3 startPosition;
        Quaternion startRotation;

        bool hasCollided;

        void Start()
        {
            startPosition = transform.position;
            startRotation = Quaternion.Euler( 17.5f , 0 , 0 );

            RoundManager.OnPhaseChanged += OnRoundPhaseChanged;
        }

        void OnCollisionEnter( Collision collision )
        {
            hasCollided = true;
        }

        void OnTriggerEnter( Collider other )
        {
            //if ( other.gameObject.layer == LayerMask.NameToLayer("Hole") )
            if ( other.CompareTag( "Finish" ) )
            {//Hit hole trigger => add score 

                int score = other.GetComponent<HoleBehavior>().ScoreReward;
                Finish_Shot( score );
            }
        }

        void OnTriggerStay( Collider other )
        {
            //if ( other.gameObject.layer == LayerMask.NameToLayer("Hole") )
            if ( other.CompareTag( "Finish" ) )
            {//Hit hole trigger => add score 

                int score = other.GetComponent<HoleBehavior>().ScoreReward;
                Finish_Shot( score );
            }
        }

        void FixedUpdate()
        {
            if ( hasCollided && RoundManager.Instance.Phase == RoundPhase.shot )
            {
                //if ( coinRigid.velocity.magnitude < 0.05f )
                if ( coinRigid.IsSleeping() )
                    Finish_Shot( 0 );
            }
        }

        void Finish_Shot( int _score )
        {
            StopAllCoroutines();
            //enabled = false;
            //Debug.Log( "Finish_Shot >> "+ _score );
            RoundManager.Instance.Start_ScorePhase( _score );
        }

        public void Throw_Coin( Vector3 _force )
        {
            coinRigid.isKinematic = false;

            coinSound.Play_CoinThrow();

            coinRigid.AddForce( _force , ForceMode.Impulse );

            RoundManager.Instance.Start_ShotPhase();

            StartCoroutine( Coroutine_ForceFinish() );
        }

        IEnumerator Coroutine_ForceFinish()
        {
            yield return new WaitForSeconds( 10f );

            Finish_Shot( 0 );
        }

        public void Reset_Coin()
        {
            hasCollided = false;
            //enabled = true;

            coinRigid.isKinematic = true;
            coinRigid.velocity = Vector3.zero;

            coinRigid.MovePosition( startPosition );
            coinRigid.MoveRotation( startRotation );
        }

        public float GetMass()
        {
            return coinRigid.mass;
        }

        void OnRoundPhaseChanged( RoundPhase _phase )
        {
            if ( _phase == RoundPhase.aim )
                Reset_Coin();
        }


        void OnDestroy()
        {
            RoundManager.OnPhaseChanged -= OnRoundPhaseChanged;
        }
    }

}