﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sapito.Main
{

    public class CoinSound : MonoBehaviour
    {
        public AudioSource audioSource;

        public AudioClip woodSound;
        public AudioClip metalSound;

        public AudioClip throwSound;

        int woodLayer, metalLayer;

        Rigidbody coinRigid;

        float volumeScale = 1f;

        void Start()
        {
            woodLayer = LayerMask.NameToLayer( "Wood" );
            metalLayer = LayerMask.NameToLayer( "Metal" );

            coinRigid = GetComponent<Rigidbody>();

            RoundManager.OnPhaseChanged += OnRoundPhaseChanged;
        }

        public void Play_CoinThrow()
        {
            PlaySound( throwSound );
        }

        void OnCollisionEnter( Collision collision )
        {
            bool hitWood = (collision.gameObject.layer & woodLayer) != 0;
            if ( hitWood )
                PlaySound( woodSound );

            bool hitMetal = (collision.gameObject.layer & metalLayer) != 0;
            if ( hitMetal )
                PlaySound( metalSound );
        }

        void PlaySound( AudioClip _clip )
        {
            float volume = Mathf.Clamp01( coinRigid.velocity.magnitude / 4f ) * volumeScale;
            audioSource.PlayOneShot( _clip , volume );
        }

        void OnDestroy()
        {
            RoundManager.OnPhaseChanged -= OnRoundPhaseChanged;
        }

        void OnRoundPhaseChanged( RoundPhase _phase )
        {
            volumeScale = ( _phase == RoundPhase.shot ) ? 1f : 0f;
        }
    }

}