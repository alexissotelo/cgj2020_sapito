﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardSetup : MonoBehaviour
{
    public GameObject holePrefab;

    public Transform holeRoot;

    public int xRows = 4;
    public int zColumns = 5;

    public float xDelta;
    public float zDelta;

    void Start()
    {
        
    }

    public void Spawn_HoleTriggers()
    {
        Vector3 originPosition = holeRoot.position;

        for ( int xRow = 0; xRow < xRows; xRow++ )
        {


            for ( int zCol = 0; zCol < zColumns; zCol++ )
            {
                Vector3 position = originPosition;
                position.x = xRow * xDelta;
                position.z = zCol * zDelta;

                var pref = Instantiate( holePrefab, position , Quaternion.identity );

                pref.transform.SetParent( holeRoot );
            }
        }
    }
}
