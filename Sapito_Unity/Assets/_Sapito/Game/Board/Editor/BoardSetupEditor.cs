﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BoardSetup))]
public class BoardSetupEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BoardSetup boardSetup = target as BoardSetup;

        if( GUILayout.Button("Spawn Holes") )
        {
            boardSetup.Spawn_HoleTriggers();
        }
    }
}
