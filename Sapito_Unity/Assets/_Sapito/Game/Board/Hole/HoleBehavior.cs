﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HoleBehavior : MonoBehaviour
{
    [SerializeField] int scoreReward = 100;

    [SerializeField] TextMeshPro scoreText;

    public ParticleSystem particles;

    public AudioSource audioSource;

    void Start()
    {
        scoreText.text = scoreReward.ToString();
    }

    void OnTriggerEnter( Collider other )
    {
        if(other.gameObject.CompareTag("Player"))
            Reward();
    }

    void Reward()
    {
        if(!audioSource.isPlaying)
            audioSource.Play();

        particles.gameObject.SetActive( true );

        Invoke( nameof( HideParticles ) , 1.5f );
    }

    void HideParticles()
    {
        particles.gameObject.SetActive( false );
    }

    public int ScoreReward { get => scoreReward; }
}
