﻿using Sapito.Main;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIMenu : MonoBehaviour
{
    public RoundManager roundManager;

    public Button playButton;

    void Start()
    {
        playButton.onClick.AddListener( OnClick_Play );
    }

    void OnClick_Play()
    {
        gameObject.SetActive( false );

        roundManager.gameObject.SetActive( true );
    }

}
